<!DOCTYPE html>
<html>
<head>
	<title>Приёмная комми</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel='stylesheet' id='style.css-css'  href='css/MDB Free/css/mdb.min.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" class="h-75" style="opacity: 1; background-image: url(&quot;img/image.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>
	



	
		
<div class="container-fluid">	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom" >
					<a href="#whypolitech2" class="">
				<img class="img-fluid" src="img/proektnay.jpg" alt="">
				<div class="card-body pt-4 mt-1">
					<p class="card-text ">Выбираем направление</p>
				</div>
					</a>
			</div>
			<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom">
						<a href="#whydoc">
				    <img class="img-fluid" src="img/studentlive.jpg" alt="">
				        <div class="card-body pt-4 mt-1">
				        <p class="card-text">Подаем документы</p>
				    </div>
						</a>
				</div>
				<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom">
						<a href="#whydocexam" class="">
				    <img class="img-fluid" src="img/praktika.jpg" alt="">
				        <div class="card-body pt-4 mt-1" >
				        <p class="card-text">Сдаем внутренние экзамены</p>
				    </div>
						</a>
				</div>
				
				<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom">
						<a href="#end" class="">
				    <img class="img-fluid" src="img/praktika.jpg" alt="">
				        <div class="card-body pt-4 mt-1" >
				        <p class="card-text">Ждем результаты</p>
				    </div>
						</a>
				</div>
		</div>
	</div>
		
	
</div>

	<div class="container-fluid mb-1" id="whypolitech2">
		<div class="row justify-content-center">
			<div>
				<h2>Факультеты</h2>
			</div>
		</div>
	</div>
	

<div class="container-fluid mb-5">	

		<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/graphics/index.php" class="">
					<img class="card-img" src="img/21.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт графики и искусства книги</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/it/index.php" class="">
					<img class="card-img" src="img/22.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет информационных технологий </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/comunication/index.php" class="">
					<img class="card-img" src="img/23.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт коммуникаций и медиабизнеса</span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/usrists/index.php" class="">
					<img class="card-img" src="img/24.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Юридический факультет</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/journals/index.php" class="">
					<img class="card-img" src="img/25.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт издательского дела и журналистики</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/economics and control/index.php" class="">
					<img class="card-img" src="img/26.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет экономики и управления</span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/transport/index.php" class="">
					<img class="card-img" src="img/27.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Транспортный факультет </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/machine/index.php" class="">
					<img class="card-img" src="img/28.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет машиностроения</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/printmedia/index.php" class="">
					<img class="card-img" src="img/29.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт принтмедиа и информационных технологий </span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/himbiotech/index.php" class="">
					<img class="card-img" src="img/30.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет химической технологии и биотехнологии </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/urbanistcs/index.php" class="">
					<img class="card-img" src="img/31.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет урбанистики и городского хозяйства </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" style="">
					<img class="card-img" src="" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0">
							<span class="card-text "></span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
</div>

	
	<div id="quoteContainer" class="container-fluid mb-5">
		<div class="container-fluid mb-1" id="whydoc">
			<div class="row justify-content-center mb-2">
				<div>
					<h2>Какие документы нужны для поступления?</h2>
				</div>
			</div>
		</div>
		<div class="row" >
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/1.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p>Паспорт или другой документ, удостоверяющий личность и гражданство абитуриента</p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/2.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p>Документ предыдуем полученном образовании:
				аттестат об окончании школы,диплом о начальном,среднем или высшем профессиональном образовании</p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/3.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p> Информацию о результатах ЕГЭ (при наличии)</p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/4.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p>2 фотографии, если при поступлении вы будете проходить дополнительные вступительные испытания</p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/5.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p>Медицинскую справку формы 086/у - для медицинских, педагогических и некоторых других специальностей и направлений;</p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-2 text-right d-none d-md-block">
				<img src="img/6.jpg" class="img-fluid" style="width:10rem; height:10rem;">
			</div>
			<div class="col-md-10">
				<p>Приписное свидетельство или военный билет (при наличии)</p>
			</div>
			
		</div>
		
	</div>	

<div class="container-fluid mb-3" id="whydocdate">
		<div class="row justify-content-center">
			<div>
				<h2>Даты</h2>
			</div>
		</div>
	</div>	
<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify mb-3">
			<h3>При приеме на места в рамках контрольных цифр по программам бакалавриата и программам специалитета по очной и очно-заочной формам обучения процедуры зачисления проводятся в следующие сроки:</h3><br>
<h4>Дата начала приема документов: 20 июня 2018 года.</h4><br>
При приеме на общие и выделенные бюджетные места и на общие места по договорам оказания платных образовательных услуг:<br>
Дата завершения приема документов от лиц, поступающих по программам магистратуры в рамках контрольных цифр: 10 августа 2018 года, 17:00.<br>
Дата завершения приема документов от лиц, поступающих по программам магистратуры на места по договорам об оказании платных образовательных услуг: 24 августа 2018 года, 17:00.<br>
Дата завершения проводимых университетом вступительных испытаний по программам магистратуры: 24 августа 2018 года.<br>
Дата окончания приема оригиналов документов установленного образца от поступающих на места за счет бюджетных ассигнований: 17 августа 2018 года, 18:00.<br>
Дата зачисления на обучение по программам магистратуры на места за счет бюджетных ассигнований: 20 августа 2018 года, не позднее 17:00.<br>
Дата окончания приема оригиналов документов установленного образца или согласия на зачисление от поступающих на места по договорам оказания платных образовательных услуг: 29 августа 2018 года, 18:00.<br>
Дата завершения зачисления на обучение по программам магистратуры на места по договорам оказания платных образовательных услуг: 30 августа 2018 года, не позднее 18:00.<br>			
<h4><br>Размещение списков поступающих на официальном сайте и на информационном стенде – 27 июля 2018 года, не позднее 17:00;</h4><br>
<h4><br> Этап приоритетного зачисления – зачисление без вступительных испытаний, зачисление на места в пределах особой квоты и целевой квоты (далее – места в пределах квот):</h4><br>
28 июля 2018 года завершается прием заявлений о согласии на зачисление от лиц, поступающих без вступительных испытаний, поступающих на места в пределах квот, если указанные лица одновременно подали заявления о приеме в две или более организаций высшего образования;
29 июля 2018 года, не позднее 18:00, издается приказ (приказы) о зачислении лиц, подавших заявление о согласии на зачисление, из числа поступающих без вступительных испытаний, поступающих на места в пределах квот;<br>
<h4><br> Зачисление по результатам вступительных испытаний на места, оставшиеся после зачисления лиц без вступительных испытаний, в пределах квот (далее – основные конкурсные места):</h4><br>
а) первый этап зачисления на основные конкурсные места – зачисление на 80% указанных мест (если 80% составляет дробную величину, осуществляется округление в большую сторону):
01 августа 2018 года, 18:00:
завершается прием заявлений о согласии на зачисление от лиц, включенных в списки поступающих на основные конкурсные места и желающих быть зачисленными на первом этапе зачисления на основные конкурсные места;
в рамках каждого списка поступающих выделяются лица, подавшие заявление о согласии на зачисление, до заполнения 80% основных конкурсных мест (с учетом округления);<br>
03 августа 2018 года, не позднее 18:00, издается приказ (приказы) о зачислении лиц, подавших заявление о согласии на зачисление, до заполнения 80% основных конкурсных мест;<br>
б) второй этап зачисления на основные конкурсные места – зачисление на 100% указанных мест:
06 августа 2018 года, 18:00:
завершается прием заявлений о согласии на зачисление от лиц, включенных в списки поступающих на основные конкурсные места;
в рамках каждого списка поступающих выделяются лица, подавшие заявление о согласии на зачисление, до заполнения 100% основных конкурсных мест;
08 августа 2018 года, не позднее 18:00, издается приказ (приказы) о зачислении лиц, подавших заявление о согласии на зачисление, до заполнения 100% основных конкурсных мест.<br>
<h4><br> Зачисление поступающих по очной форме обучения на места по договорам об оказании платных образовательных услуг производится по следующему графику:</h4><br>
28 августа 2018 года, 18:00 – завершение приема согласий на зачисление и завершение заключения договоров об оказании платных образовательных услуг:
30 августа 2018 года, не позднее 18:00:
завершение издания и размещения на официальном сайте и информационном стенде приемной комиссии приказов о зачислении на места по договорам об оказании платных образовательных услуг.
первое про подачу, второе кто когда зачислен
Места, освободившиеся в результате отчисления лиц, зачисленных на обучение на предшествующих этапах, добавляются к основным конкурсным местам по тем же условиям поступления.<br>
<h4><br> Зачисление поступающих по заочной форме обучения на места в пределах контрольных цифр производится по следующему графику:</h4><br>
26 сентября 2018 года, 18:00 – завершение приема согласий на зачисление от лиц (одновременно подавших заявления о приеме в соответствии с двумя и более подпунктами пункта 2.6.5 настоящих Правил), поступающих:
на места в пределах особой квоты;
на места в пределах целевой квоты;
27 сентября 2018 года, 18:00 – завершение приема согласий на поступление от поступающих на места в пределах контрольных цифр.
28 сентября 2018 года, не позднее 17:00:
издание и размещение на официальном сайте и информационном стенде приемной комиссии приказов о зачислении в пределах контрольных цифр поступающих без вступительных испытаний; поступающих по особой квоте; поступающих по целевой квоте; на основные места.
Конкурсные списки поступающих обновляются ежедневно со дня начала приема документов до издания приказов о зачислении.<br>
<h4><br> Зачисление поступающих по заочной форме обучения на места по договорам об оказании платных образовательных услуг производится по следующему графику:</h4><br>
29 октября 2018 года, 18:00 – завершение приема согласия на зачисление поступающих по договорам об оказании платных образовательных услуг.
30 октября 2018 года, не позднее 17:00 – издание и размещение на официальном сайте и информационном стенде приемной комиссии приказов о зачислении поступающих по договорам об оказании платных образовательных услуг и представивших в установленный срок согласие на зачисление.
Приемная комиссия Московского Политеха устанавливает следующие сроки проведения приемной кампании в магистратуру в 2018 году:<br>

			</div>
	<div class="container-fluid mb-1" id="whydocdate">
		<div class="row justify-content-center">
			<div>
				<h2>Адреса премной комиссии</h2>
			</div>
		</div>
	</div>	
	<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify mb-5">
		Адрес: Москва, Большая Семеновская ул., д. 38, корпус Н, 4 этаж, аудитория Н-407<br>
	Телефон на Б. Семеновскую: (495) 223-05-23, <br>
	добавочный 1640<br>

	Адрес: Москва, ул. Прянишникова,д. 2а <br>
	Телефон на Прянишникова: (495) 223-05-23, <br>
	добавочный 1630<br>
	</div>
	<div class="container-fluid mb-1" id="whydocexam">
		<div class="row justify-content-center">
			<div>
				<h2>ПРАВИЛА ПРОВЕДЕНИЯ ВСТУПИТЕЛЬНЫХ ИСПЫТАНИЙ </h2>
			</div>
		</div>
	</div>	
	<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify">
		
В 2018 году университет проводит вступительные испытания по математике, физике, информатике и информационно-коммуникативным технологиям (ИКТ), русскому языку, обществознанию, истории и биологии в письменной форме с использованием бланкового тестирования (письменное тестирование) для категорий поступающих, перечисленных ниже. Язык проведения всех вступительных испытаний – русский.<br>

Отдельные категории поступающих на базе среднего общего образования могут по своему усмотрению поступать на обучение по результатам общеобразовательных вступительных испытаний, проводимых организацией высшего образования самостоятельно:<br>

а) лица с ограниченными возможностями здоровья, дети-инвалиды, инвалиды;<br>

б) иностранные граждане;<br>

в) лица, прошедшие государственную итоговую аттестацию по образовательным программам среднего общего образования не в форме ЕГЭ (в том числе в иностранных образовательных организациях) в течение 1 года до дня завершения приема документов и вступительных испытаний включительно.<br>

<br><h4>Даты проведения вступительных испытаний в 2018 году:</h4>

1 поток - с 11 по 26 июля 2018 года<br> 2 поток - с 15 по 27 августа 2018 года<br> 3 поток - с 14 по 25 сентября 2018 года<br> 4 поток - с 12 по 25 октября 2018 года<br>

<br><h4>Структура экзаменационных заданий и время продолжительности вступительных испытаний:</h4>

1. Математика (в каждом задании с выбором вариантов ответа может

быть только один правильный ответ):

Продолжительность: 120 минут.

Структура задания: 15 заданий, из которых 10 с вариантами ответов для выбора, 5 – с открытой формой ответа.<br>

2. Физика (в каждом задании с выбором вариантов ответа может быть

только один правильный ответ):

Продолжительность: 150 минут.

Структура задания: 20 заданий, из которых 15 с вариантами ответов для выбора, 5 – с открытой формой ответа.<br>

3. Информатика и ИКТ (в каждом задании с выбором вариантов ответа

может быть только один правильный ответ):

Продолжительность: 120 минут.

Структура задания: 28 заданий, из которых 18 с вариантами ответов для выбора, 10 – с открытой формой ответа.<br>

4. Русский язык (в каждом задании с выбором вариантов ответа может

быть только один правильный ответ):

Продолжительность: 90 минут.

Структура задания: 40 заданий с вариантами ответов для выбора; часть «а» - Орфография, часть «б» - Пунктуация, часть «в» - Культура речи.<br>

5. Обществознание (в каждом задании с выбором вариантов ответа может

быть только один правильный ответ):

Продолжительность: 210 минут.

Структура задания: 60 заданий по обществознанию.<br>

6. История (в каждом задании с выбором вариантов ответа может быть

только один правильный ответ):

Продолжительность: 210 минут.

Структура задания: 60 заданий по истории.<br>

7. Биология (в каждом задании с выбором вариантов ответа может быть

только один правильный ответ):

Продолжительность: 90 минут.

Структура задания: 25 заданий по биологии. 
	</div>
	
	<div class="container-fluid mb-1" id="whydocdate">
		<div class="row justify-content-center">
			<div>
				<h2>Конкурс</h2>
			</div>
		</div>
	</div>	
	<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify">
			<h4>Проходные баллы определяется по итогам конкурса. Они 
меняются каждый год и определяются только в процессе зачисления. 
В качестве ориентира вы можете посмотреть проходные баллы 
на факультеты за прошлый год.</h4><br>
<h4>
Абитуриенты, которые поступают по квотам, не принимают 
участие в общем конкурсе, но участвуют в конкурсе в рамках 
своей квоты. Для этого они также должны набрать количество 
баллов, равное или превышающее минимальное значение, 
установленное вузом.</h4><br>

<h4>Победители и призеры олимпиад школьников, входящих в 
перечень Минобрнауки России, поступают в Московский Политех
без вступительных испытаний.</h4><br>
	</div>
	
<div class="container-fluid" id="end">	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom" >
					<a href="http://mospolytech.ru/storage/1679091c5a880faf6fb5e6087eb1b2dc/files/2018/prilozhenie/Prilozhenie_2.5_KTsP_Moskva.pdf">
				<div class="card-body pt-4 mt-1">
					<p class="card-text ">Кол-во мест для приема на обучение на 2018г</p>
				</div>
					</a>
			</div>
			<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom">
						<a href="http://mospolytech.ru/index.php?id=2339">			   
				        <div class="card-body pt-4 mt-1">
				        <p class="card-text">Пофамильный рейтинговые списки абитуриентов</p>
				    </div>
						</a>
				</div>
				<div class="card border-0 p-md-5 p-lg-5 p-sm-1 mb-md-5 mb-lg-5 mb-sm-1 bg-white rounded shadow-sm view zoom">
						<a href="http://mospolytech.ru/storage/1679091c5a880faf6fb5e6087eb1b2dc/files/otchet/otchet_priema_2017.pdf" class="">
				        <div class="card-body pt-4 mt-1" >
				        <p class="card-text">Результаты приема университета на бюджет в прошлом году</p>
				    </div>
						</a>
				</div>
				
		</div>
	</div>
		
	
</div>
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>