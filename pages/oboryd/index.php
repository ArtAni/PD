<!DOCTYPE html>
<html>
<head>
	<title>Электрооборудование</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/UU-cs_vWXX0.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>

	<div style="height: 3%!important"></div>

	
	<div class="container-fluid">
		<div class="col-12 text-center" style="color: #54a4d5;"><h1><b>Электрооборудование и промышленная электроника</h1></div>
			<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify"><b style="font-size: 150%;">
В настоящее время на кафедре имеются все необходимые составляющие для успешного прохождения учебы:необходимая учебная инфраструктура (современные аудитории, оснащенные интерактивным оборудованием, дисплейные классы, лаборатории, стенды).Для повышения уровня знаний студентов на кафедре организованы научные лаборатории, где работает научный кружок. В распоряжении студентов имеется современная механическая мастерская. В связи с потребностью в большом количестве специалистов- диагностов на кафедре развивается материальная база диагностических лабораторий, оснащение их современным оборудованием. Идет совершенствование учебного процесса путем расширения использования компьютеров, мультимедийного оборудования, приобретение специализированных учебных стендов, оборудования и моделирующих программ. На кафедре имеется научная библиотека, имеющая более 3-х тысяч томов книг по электрооборудованию, по электронике и электротехнике. При желании студенты могут заниматься в мини читальном зале. Также в интерактивных аудиториях кафедры имеется доступ к сети интернет и всем электронным ресурсам Московского Политеха.На кафедре «Электрооборудование и промышленная электроника» происходит обновление учебно-методических курсов, что позволяет быть в тренде современных веяний инженерно-технической науки в области электрооборудования и промышленной электроники. Благодаря этому студенты кафедры имеют возможность получать самые актуальные знания по своему профилю, что в дальнейшем позволит им реализовать полученные компетенции при трудоустройстве.	</div>
	</div>
		<div style="height: 2em;"></div>
	
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>