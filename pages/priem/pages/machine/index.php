<!DOCTYPE html>
<html>
<head>
	<title>Факультет машиностроения</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel='stylesheet' id='style.css-css'  href='css/MDB Free/css/mdb.min.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/28.jpg&quot;);">
		<div id="bg_pattern"></div>
	</div>
    
    <div class="container-fluid mb-5">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h1 class="text">Факультет машиностроения</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-10">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Код</th>
                            <th scope="col">Направление</th>
                            <th scope="col">Встпительные<br>испытания</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3" scope="row">Бакалавриат и специалитет</th>
                        </tr>
                        <tr>
                            <th scope="row">15.05.01</th>
                            <td><a class="text" href="pages/15.05.01.html">Проектирование технологических машин и комплексов</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">15.03.01.01</th>
                            <td><a class="text" href="pages/15.03.01.01.html">Машиностроение (машины и технологии обработки материалов давлением, оборудование и технологии сварочного производства, машины и технологии высокоэффективных процессов обработки)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">15.03.04.02</th>
                            <td><a class="text" href="pages/15.03.04.02.html">Автоматизация технологических процессов и производств (роботизированные комплексы)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">15.03.05</th>
                            <td><a class="text" href="pages/15.03.05.html">Конструкторско-технологическое обеспечение машиностроительных производств (технологическое обеспечение производства современных машин)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">22.03.01.01</th>
                            <td><a class="text" href="pages/22.03.01.01.html">Материаловедение и технологии материалов (перспективные материалы в инновационной технике)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">22.03.02</th>
                            <td><a class="text" href="pages/22.03.02.html">Металлургия (инновации в металлургии)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">27.03.01</th>
                            <td><a class="text" href="pages/27.03.01.html">Стандартизация и метеорология</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">27.03.02.01</th>
                            <td><a class="text" href="pages/27.03.02.01.html">Управление качеством (инженерные методы управления качеством)</a></td>
                            <td>
                                <ol>
                                    <li>Информатика</li>
                                    <li>Математика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">27.03.04.01</th>
                            <td><a class="text" href="pages/27.03.04.01.html">Управление в технических системах (электронные системы управления)</a></td>
                            <td>
                                <ol>
                                    <li>Информатика</li>
                                    <li>Математика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">27.03.05</th>
                            <td><a class="text" href="pages/27.03.05.html">Инноватика (аддитивные технологии)</a></td>
                            <td>
                                <ol>
                                    <li>Информатика</li>
                                    <li>Математика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">29.03.04</th>
                            <td><a class="text" href="pages/29.03.04.html">Технология художественной обработки материалов</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Русский язык</li>
                                    <li>Рисунок геометрических фигур (доп. внутривузовское испытание)</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" scope="row">Магистратура</th>
                        </tr>
                        <tr>
                            <th scope="row">15.04.01</th>
                            <td><a class="text" href="pages/15.04.01.html">Машиностроение (технологический инжиниринг в обработке материалов давлением, машины и технологии литейного производства, оборудование и технология сварочного производства, аддитивное производство, управление технологическими проектами в машиностроении, машины и технологии высокоэффективных процессов обработки) </a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">15.04.05</th>
                            <td><a class="text" href="pages/15.04.05.html">Конструкторско-технологическое обеспечение машиностроительных производств (технологическое обеспечение цифрового производства)</a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">27.04.01</th>
                            <td><a class="text" href="pages/27.04.01.html">Стандартизация и метрология (стандартизация и метрология в машиностроительном производстве)</a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">27.04.02.01</th>
                            <td><a class="text" href="pages/27.04.02.01.html">Управление качеством (управление качеством в высокотехнологичных производствах) </a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">27.04.04.01</th>
                            <td><a class="text" href="pages/27.04.04.01.html">Управление в технических системах (управление в робототехнических системах, автономные информационные управляющие системы) </a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
    </div>
	<?php include("/home/std/site/template/footer.php");  ?>

</body>
</html>