<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0.5, maximum-scale=2, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Главная</title>
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="css/mdb.min.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<?php include("/home/std/site/template/header.php");  ?>
<main class="">
	<div id="headerImageContainer" class="p-0 bg-dark">
			<img id="headerImage" src="img/headerImage.jpg" class=""/>
			<div id="logoImageContainer" class="text-center">
				<img id="logoImage" src="img/top_sh.png" class=""/>
			</div>
	</div>
	<div id="quoteContainer" class="container-fluid mb-5">
		<div class="row">
			<div class="col-md-3 d-none d-md-block">
				<img src="img/Kub.png" class="img-fluid" />
			</div>
			<div class="col-md-6" >
				<q class="blockquote">Тот, кто учится не размышляя, впадет в заблуждение. Тот, кто размышляет, не желая учится, окажется в затруднении</q>
			</div>
			<div class="col-md-3 d-none d-md-block">
				<img src="img/apple.png" class="img-fluid"/>
			</div>
		</div>
	</div>
	<div class="container-fluid mb-5" id="whypolitech">
		<div class="row">
			<div class="offset-1 justify-content-start">
				<h2 class="border-bottom">Почему политех?</h2>
			</div>
		</div>
		<div id="whyContent" class="row">
			<div class="col-md-3">
				<div class="">
					<span>2193</span><br>
					<span>бюжетных мест</span>
				</div>
				<div class="">
					<span>2047</span><br>
					<span>платных мест</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="">
					<span>до 50%</span><br>
					<span>скидка на обучение</span>
				</div>
				<div class="">
					<span>100</span><br>
					<span>образовательных программ</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="">
					<span>169</span><br>
					<span>компаний партнёров</span>
				</div>
				<div class="">
					<span>77</span><br>
					<span>кафедр</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="">
					<span>10</span><br>
					<span>общежитий</span>
				</div>
				<div class="">
					<span>4822</span><br>
					<span>места для проживания</span>
				</div>
			</div>
		</div>
	</div>
<div class="container-fluid mb-5">	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/1proekt/index.php" class="">
					<img class="img-fluid" src="img/proektnay.jpg" alt=""/>
					<div class="card-body">
						<span class="card-text">Проектная деятельность</span>
					</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/vneychebka/index.php" class="">
				    <img class="img-fluid" src="img/studentlive.jpg" alt=""/>
				     <div class="card-body">
				        <span class="card-text">Разнообразная студенческая жизнь</span>
				    </div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/praktika/index.php" class="">
				    <img class="img-fluid" src="img/prepod.jpg" alt=""/>
				    <div class="card-body" >
						<span class="card-text">Практика в лидирующих компаниях и программа обмена</span>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/worldskills/index.php" class="">
					<img class="img-fluid" src="img/worldskills.jpg" alt=""/>
					<div class="card-body">
						<span class="card-text ">Экзамены в формате WorldSkills</span>
					</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/obshag/index.php" class="">
					<img class="img-fluid" src="img/obshaga.jpg" alt=""/>
				    <div class="card-body">
				        <span class="card-text">Современные общежития квартирного, блочного и коридорного типа</span>
				    </div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/links/index.php" class="">
					<img class="img-fluid" src="img/web.jpg" alt=""/>
				    <div class="card-body" >
				        <span class="card-text">Полезные ссылки</span>
				    </div>
				</a>
			</div>
		</div>
	</div>
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/facultets/index.php" class="">
					<img class="img-fluid" src="img/facultet.jpg" alt=""/>
					<div class="card-body">
						<span class="card-text ">Направления</span>
					</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/priem/index.php" class="">
					<img class="img-fluid" src="img/sport.jpg" alt=""/>
				        <div class="card-body">
							<span class="card-text">Прием</span>
						</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="http://mospolytech.ru/index.php?id=4275" class="">
					<img class="img-fluid" src="img/praktika.jpg" alt=""/>
				        <div class="card-body" >
							<span class="card-text">Абитуриент Online</span>
						</div>
				</a>
			</div>
		</div>
	</div>
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/univer/index.php" class="">
					<img class="img-fluid" src="img/univer1.jpg" alt=""/>
					<div class="card-body">
						<span class="card-text ">Об университете</span>
					</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/oboryd/index.php" class="">
					<img class="img-fluid" src="img/oborydovbanie.jpg" alt=""/>
				        <div class="card-body">
							<span class="card-text">Современное обуродование</span>
						</div>
				</a>
			</div>
			<div class="card view zoom">
				<a href="pages/trud/index.php" class="">
					<img class="img-fluid" src="img/trud.jpg" alt=""/>
				        <div class="card-body" >
							<span class="card-text">Высокое трудоустройсво наших выпускников</span>
						</div>
				</a>
			</div>
		</div>	
	</div>
</div>
</main>
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>