<!DOCTYPE html>
<html>
<head>
	<title>ХимБиоТех</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel='stylesheet' id='style.css-css'  href='css/MDB Free/css/mdb.min.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/30.jpg&quot;);">
		<div id="bg_pattern"></div>
	</div>
    
    <div class="container-fluid mb-5">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h1 class="text">Факультет химической технологии и биотехнологии</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-10">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Код</th>
                            <th scope="col">Направление</th>
                            <th scope="col">Встпительные<br>испытания</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3" scope="row">Бакалавриат и специалитет</th>
                        </tr>
                        <tr>
                            <th scope="row">19.03.01</th>
                            <td><a class="text" href="pages/19.03.01.html">Биотехнология</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Биология</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">18.05.01</th>
                            <td><a class="text" href="pages/18.05.01.html">Химическая технология энергонасыщенных материалов и изделий</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">16.03.03</th>
                            <td><a class="text" href="pages/16.03.03.html">Холодильная, криогенная техника и системы жизнеобеспечения</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">18.03.02</th>
                            <td><a class="text" href="pages/18.03.02.html">Энерго- и ресурсосберегающие процессы химической технологии, нефтехимии и биотехнологии</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">20.03.01</th>
                            <td><a class="text" href="pages/20.03.01.html">Техносферная безопасность</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" scope="row">Магистратура</th>
                        </tr>
                        <tr>
                            <th scope="row">16.04.03</th>
                            <td><a class="text" href="pages/16.04.03.html">Холодильная, криогенная техника и системы жизнеобеспечения</a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">20.04.01.01</th>
                            <td><a class="text" href="pages/20.04.01.01.html">Техносферная безопасность (надзорная и инспекционная деятельность в сфере труда)</a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">20.04.01.02</th>
                            <td><a class="text" href="pages/20.04.01.02.html">Техносферная безопасность (оценка и управление экологическими рисками) </a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </div>
    </div>
	<?php include("/home/std/site/template/footer.php");  ?>

</body>
</html>