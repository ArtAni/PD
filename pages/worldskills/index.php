<!DOCTYPE html>
<html>
<head>
	<title>Worldskills</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/UU-cs_vWXX1.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>

	<div style="height: 3%!important"></div>

	
	<div class="container-fluid">
		<div class="offset-md-5 col-md " style="color: #54a4d5;"><h1><b>World skills</b></h1></div>
			<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify"><b style="font-size: 150%;">
Демонстрационный экзамен по стандартам Ворлдскиллс – это форма государственной итоговой аттестации выпускников по программам среднего профессионального образования образовательных организаций высшего и среднего профессионального образования, которая предусматривает:
моделирование реальных производственных условий для демонстрации выпускниками профессиональных умений и навыков;
независимую экспертную оценку выполнения заданий демонстрационного экзамена, в том числе экспертами из числа представителей предприятий;
определение уровня знаний, умений и навыков выпускников в соответствии с международными требованиями.
Демонстрационный экзамен по стандартам Ворлдскиллс Россия проводится с целью определения у студентов и выпускников уровня знаний, умений, навыков, позволяющих вести профессиональную деятельность в определенной сфере и (или) выполнять работу по конкретным профессии или специальности в соответствии со стандартами Ворлдскиллс Россия.
Включение формата демонстрационного экзамена в процедуру государственной итоговой аттестации обучающихся профессиональных образовательных организаций – это модель независимой оценки качества подготовки кадров, содействующая решению нескольких задач системы профессионального образования и рынка труда без проведения дополнительных процедур.		</b>
			</div>
	</div>
		<div style="height: 2em;"></div>
	
<?php include("/home/std/site/template/footer.php");  ?>

    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>