<!DOCTYPE html>
<html>
<head>
	<title>Link</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/1.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>

	<div style="height: 3%!important"></div>

	
	<div class="container-fluid">
		<div class="col-12 text-center" style="color: #54a4d5;"><h1><b>Полезные ссылки</b></h1></div>
			<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12"><b style="font-size: 150%;">
				<a>https://vk.com/moscowpolytech</a> - Московский политех<br>

				<a>https://vk.com/polytech_abiturient</a> - Абитуриенты | Московский Политех<br>

				<a>http://mospolytech.ru/index.php?id=24</a> -Факультеты и институты<br>

				<a>http://mospolytech.ru/index.php?id=101</a> - Абитуриенту Направления и специальности<br>

				<a>https://vk.com/abiturum</a> - ABITURIENTUM <br>

				<a>https://vk.com/club25103786</a> -Афиша Московского Политеха<br>

				<a>https://vk.com/profkommospolytech</a> -Профорганизация Московского Политеха<br>

				<a>https://vk.com/polytech_media </a>- Высшая школа печати и медиаиндустрии (быв. МГУП)<br>

				<a>http://mgup.ru/article/609</a> - Институт графики и искусства книги имени В.А.Фаворского<br>

				<a>http://mgup.ru/article/608</a> - Институт коммуникаций и медиабизнеса<br>

				<a>http://mgup.ru/article/607</a> - Институт издательского дела и журналистики<br>

				<a>http://mgup.ru/article/605</a> - Институт принтмедиа и информационных технологий<br>

				<a>http://mgup.ru</a> - Высшая школа печати и медиаиндустрии Московского Политеха<br>

				<a>https://www.instagram.com/polytech_media/</a> - Высшая школа печати   и медиа<br>

				<a>https://www.facebook.com/polytech.media</a> - Высшая школа печати и медиаиндустрии Московского Политеха <br>

				<a>https://vk.com/public86619457</a> -  Эхо Политеха | Московский Политех<br>

				<a>https://twitter.com/polytech_media</a> - твиттер Высшей школы Печати и Медиа<br>

				<a>https://vk.com/hs_ml</a> - Высшая Школа Управления и Права<br>

				<a>http://mospolytech.ru/index.php?id=4635</a>   - факультет технологического предпринимательства<br>

				<a>http://mospolytech.ru/index.php?id=5219</a> - Юридический факультет<br>

				<a>http://mospolytech.ru/index.php?id=5220</a> - Факультет экономики и управления<br>

				<a>https://vk.com/mpu_mc</a> - Бренд-менеджмент в рекламе и PR <br>

				<a>http://mospolytech.ru/kaf/mk/</a> - Кафедра маркетинговые коммуникации<br>

				<a>https://www.instagram.com/market.community/</a> - инста кафедры<br>

				<a>https://vk.com/club156453495</a> - Московский Политех, кафедра "ЭиО"<br>

				<a>http://mospolytech.ru/index.php?id=383</a> -Кафедра “Экономика и организация”<br>

				<a>https://vk.com/management_mospolytech</a> - Кафедра "Менеджмент" Московский Политех<br>

				<a>https://www.instagram.com/management_mospolytech/</a> - инста Кафедра Менеджмент <br>

				<a>https://vk.com/prof_hsml</a> -Профбюро "Высшей школы Управления и Права"<br>

				<a>https://vk.com/fit.mospolytech</a> - ИТ-факультет Московского Политеха<br>

				<a>http://mospolytech.ru/index.php?id=4594</a> - Факультет информационных технологий<br>

				<a>https://vk.com/transportfaculty</a> - Транспортный факультет Московского политеха<br>

				<a>http://mospolytech.ru/index.php?id=3922</a> - Транспортный факультет<br>

				<a>https://vk.com/fm.mospolytech</a> - Факультет Машиностроения | Московский Политех<br>

				<a>http://mospolytech.ru/index.php?id=4595</a> -Факультет Машиностроения<br>

				<a>http://mospolytech.ru/index.php?id=4633</a> - факультет химической технологии и биотехнологии<br>

				<a>http://mospolytech.ru/index.php?id=4634</a> - Факультет урбанистики и городского хозяйства<br>

				<a>http://mospolytech.ru/index.php?id=4449</a> - Факультет базовых компетенций<br>

				<a>http://mospolytech.ru/index.php?id=92</a> - Дополнительное образование<br>

				<a>http://mospolytech.ru/index.php?id=3673</a> - Инженерная школа (факультет)<br>

				<a>https://vk.com/magistratura_mospolytech</a> - IT-магистратура Московский Политех<br>

				<a>https://vk.com/cheer_full.drive</a> - Команда По Черлидингу "FULL DRIVE" МПУ (МАМИ)<br>

				<a>https://www.instagram.com/fulldrive.cheer/</a> - инста FULL DRIVE<br>

				<a>https://vk.com/echopolytecha</a> - Эхо Политеха | Московский Политех<br>

				<a>https://vk.com/kvn.mospolytech</a> - Местная лига КВН Московского Политеха<br>

				<a>https://vk.com/tm_mospolytech</a> - Творческая мастерская<br>

				<a>https://vk.com/freedanceum</a> - FREE DANCE FAMILY<br>

				<a>https://www.instagram.com/freedancefamily/</a> - инста FREE DANCE FAMILY<br>

				<a>https://www.youtube.com/channel/UC3miGBdWN6TZjGRvUVyYO_Q</a> -ютуб FREE DANCE FAMILY<br>

				<a>https://vk.com/club5563729</a> - Профорганизация Московского Политеха<br>

				<a>https://profkomum.ru</a> - сайт Профсоюзной организации<br>

				<a>https://ru-ru.facebook.com/people/</a>  Профорганизация-Московского-Политеха/100010535613164 - Facebook профорганизации мосполитехаэ<br>

				<a>https://vk.com/kuratortv</a> -КураторТВ - Московский Политех<br>

			</div>
	</div>
		<div style="height: 2em;"></div>
	
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>