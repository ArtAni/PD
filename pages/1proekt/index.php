<!DOCTYPE html>
<html>
<head>
	<title>Проектная деятельность</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" class="h-75 container-fluid mb-5" style="height: 37em; opacity: 1; background-image: url(&quot;img/UU-cs_vWXX0.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>





	
	<div class="container-fluid mb-5">
		<div class="offset-md-4 col-md " style="color: #54a4d5;"><h1><b>Проектная деятельность</b></h1></div>
			<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify"><b style="font-size: 150%;color: #303030">
			 Обязательная дисциплина в Московском Политехе с первых дней учёбы вы погрузитесь в особую, творческую образовательную среду: работая над проектами, вы не только освоите востребованные профессиональные навыки, но и научитесь командной работе, лидерству, умению работать с информацией, соревноваться и побеждать.
Студенты применяют теорию сразу, а не когда-нибудь "потом", во время летней практики или на стажировке. В начале семестра они выбирают индустриальный проект, разбираются с техническим заданием и берутся за дело. В командах, где каждый работает головой и руками с ориентацией на результат, появляются свежие идеи и интересные решения. Итоги работы оценивают отраслевые эксперты.
Чем полезно такое обучение? Ты получаешь не оценки за зубрёжку предметов, а профессиональные компетенции: не «сдал зачёт по черчению», а получил отраслевой сертификат по компьютерному моделированию.
Вместо того чтобы оттачивать навык конспектирования, ты научишься делать продукт, который работает: сайты, облачные сервисы, приложения, узлы спутника, электробайки, систему закваски - зависит от образовательной программы, которую выберешь.
Почему и как это работает? Вы поступаете на образовательные программы (ОП). Содержание ОП разработали их руководители – люди из индустрий, которых пригласил Политех. Они знают, в каком направлении и зачем развиваются их отрасли. Руководители подбирают преподавателей, пишут учебные планы и несут за них персональную ответственность. Благодаря такому подходу, ты обучаешься тому, что нужно индустрии сейчас и в перспективе.
			</b>
			</div>
	</div>
		<div style="height: 2em;"></div>
	
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>