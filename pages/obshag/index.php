<!DOCTYPE html>
<html>
<head>
	<title>Студенческий городок</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" class="container-fluid mb-5" style="height: 37em; opacity: 1; background-image: url(&quot;img/obshagt.jpg&quot;);">
		<div id="bg_pattern"></div>

	</div>

	<div style="height: 3%!important"></div>

	<div class="container">
		<div class="row text-center">
			<div class="offset-md-1 col-md " style="color: #54a4d5;"><h1><b>Студенческий городок</h1></div>
		</div>
		<div class="row">
			<div class="col"><p><b>Комплекс десяти общежитий, расчитанный на 4822 мест, с широко развитой инфраструктурой и необходимыми условиями для проживания и отдыха студентов</b></p></div>
		</div>
	</div>
    <div class="container-fluid">
        <div class="" style="border: none;">
                <img class="card-img-top" src="img/123.png" alt="Card image cap">
        </div>
    </div>
	<div class="container">
		<div class="row">
			<div class="col-12"><h1 style="color: #6fb4dc">Типы объектов студенческого городка:</h1></div>
		</div>
		<ul class="row text-left">
			<li class="col-12">блочный – раздельные комнаты, расположенные блоками (по 2 комнаты) с отдельным санитарным узлом и душевой комнатой. Кухня на этаже.</li>
			<li class="col-12">коридорный – раздельные комнаты, расположенные вдоль коридора. Санитарный узел и кухня на этаже.</li>
			<li class="col-12">квартирный – раздельные комнаты, расположенные блоками (по 2-3 комнаты) с отдельным санузлом, душевой и кухней.</li>
			<li class="col-12">смешанный – наличие блочного и коридорного типа объектов.</li>
		</ul>
		<div class="row">
			<div class="col-12"><h1 style="color: #6fb4dc">Инфраструктура студенческого городка:</h1></div>
		</div>
		<ul class="row text-left">
			<li class="col-12">комнаты отдыха;</li>
			<li class="col-12">котнаты для самостоятельных занятий;</li>
			<li class="col-12">спортивный и тренажерный зал;</li>
			<li class="col-12">камера хранения;</li>
			<li class="col-12">постирочная комната;</li>
			<li class="col-12">гладильная комната;</li>
			<li class="col-12">буфет и столовая;</li>
			<li class="col-12">детская комната;</li>
			<li class="col-12">комната для рзмещения родителей;</li>
			<li class="col-12">спортивная площадка;</li>
			<li class="col-12">бесплатная сеть Wi-Fi.</li>
		</ul>
		<div class="row">
			<div class="col-12"><h1 style="color: #6fb4dc">Современные системы безопасности:</h1></div>
		</div>
		<ul class="row text-left">
			<li class="col-12">система контроля и управления доступом;</li>
			<li class="col-12">система речевого оповещения (на 3-х языках);</li>
			<li class="col-12">система видеонаблюдения;</li>
			<li class="col-12">пожарная сигнализация;</li>
		</ul>
		<div class="row">
			<div class="col-12"><h1 style="color: #6fb4dc">Имущество, предназначенное для коллективного и индивидуального использования:</h1></div>
		</div>
		<ul class="row text-left">
			<li class="col-12">Гардероб(на комнату)</li>
			<li class="col-12">кровать (на каждого проживающего);</li>
			<li class="col-12">тумбочка (на каждого проживающего);</li>
			<li class="col-12">полка (на каждого проживающего);</li>
			<li class="col-12">письменный стол (на каждого проживающего);</li>
			<li class="col-12">розетка 220В;</li>
			<li class="col-12">зеркало (на комнату);</li>
			<li class="col-12">постельные принадлежности (на каждого проживающего): матрас (90x200 см), подушка (70x70 см), одеяло (150x200 см), комплект постельного белья.</li>
		</ul>
		<div class="row">
			<div class="col-12"><h1 style="color: #6fb4dc">Стоимость проживания для абитуриентов:</h1></div>
			<div class="col-12"><p>На время подачи документов и сдачи вступительных испытаний абитуриентам и их родственникам (законным представителям) может быть предоставлено койко-место в общежитии.</p></div>
		</div>
    </div>
    <div class="container-fluid">
		<table class="table text-center">
			<thead>
				<tr>
					<th scope="col">Общежития № 1, 4, 5, 6</th>
					<th scope="col">Общежития № 2, 3, 7, 9, 10</th>
					<th scope="col">Общежитие № 8</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>1000 руб. в сутки</td>
					<td>800 руб. в сутки</td>
					<td>1600 руб. в сутки</td>
				</tr>
			</tbody>
		</table>
	</div>
    <div class="container">
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №1</h4>
            <p class="col-12 text-center">ст. м. «Электрозаводская», ул. М. Семёновская, д. 12</p>
            
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №2</h4>
            <p class="col-12 text-center">ст. м. «Первомайская», ул. 7–я Парковая, д. 9/26</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №3</h4>
            <p class="col-12 text-center">ст. м. «Дубровка», ул. 1-я Дубровская, д. 16а</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №4</h4>
            <p class="col-12 text-center">ст.м. Петровско-Разумовская, станция Бескудниково Савеловской ж/д, ул. 800-летия Москвы, д. 28 </p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №5</h4>
            <p class="col-12 text-center">ст. м. Войковская, Дмитровская, Петровско-Разумовская, ст. Коптево МЦК, ул. Михалковская, д. 7, корп. 3</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №6</h4>
            <p class="col-12 text-center">ст. м. «ВДНХ», ул. Б. Галушкина, д. 9</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №7</h4>
            <p class="col-12 text-center">ст. м. «ВДНХ», ул. Павла Корчагина, д. 20 А, к. 3</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №8</h4>
            <p class="col-12 text-center">ст. м. «ВДНХ», Рижский проезд, д. 15, к. 2</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №9</h4>
            <p class="col-12 text-center">ст. м. «ВДНХ», Рижский проезд, д. 15, к. 1</p>
        </div>
        <div class="row">
            <h4 class="col-12 text-center">Адрес общежития №10</h4>
            <p class="col-12 text-center">ст. м. «Сокол», 1-й Балтийский переулок, д. 6/21 корп. 3</p>    
        </div>
        <div class="" style="border: none;">
            <img class="card-img-top" src="img/wYM5Yjfl4C4.jpg" alt="Card image cap">
        </div>
    </div>
<?php include("/home/std/site/template/footer.php");  ?>
</body>
</html>