<!DOCTYPE html>
<html>
<head>
	<title>Институт принтмедиа и информационных технологий</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel='stylesheet' id='style.css-css'  href='css/MDB Free/css/mdb.min.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/29.jpg&quot;);">
		<div id="bg_pattern"></div>
	</div>
    
    <div class="container-fluid mb-5">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h1 class="text">Институт принтмедиа и информационных технологий</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-10">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Код</th>
                            <th scope="col">Направление</th>
                            <th scope="col">Встпительные<br>испытания</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3" scope="row">Бакалавриат и специалитет</th>
                        </tr>
                        <tr>
                            <th scope="row">09.03.02</th>
                            <td><a class="text" href="pages/09.03.02.html">Информационные системы и технологии</a></td>
                            <td>
                                <ol>
                                    <li>Информатика</li>
                                    <li>Математика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">15.03.02</th>
                            <td><a class="text" href="pages/15.03.02.html">Технологические машины и оборудование (принтмедиасистемы и комплексы)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">15.03.04.01</th>
                            <td><a class="text" href="pages/15.03.04.html">Автоматизация технологических процессов и производств (компьютерные системы в печатных и электронных средствах информации)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">22.03.01.02</th>
                            <td><a class="text" href="pages/22.03.01.html">Материаловедение и технологии материалов (материаловедение и наномодификация материалов для принтмедиаиндустрии)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">29.03.03</th>
                            <td><a class="text" href="pages/29.03.03.html">Технология полиграфического и упаковочного производства (дизайн и технология полиграфического производства, дизайн и технология создания упаковки, полиграфические технологии в нано- и микроэлектронике)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Физика</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="3" scope="row">Магистратура</th>
                        </tr>
                        <tr>
                            <th scope="row">09.04.01.02</th>
                            <td><a class="text" href="pages/09.04.01.html">Информатика и вычислительная техника (прикладная медиаинформатика, электронные издания и мультимедийные системы)</a></td>
                            <td>Междисциплинарный экзамен</td>
                        </tr>
                        <tr>
                            <th scope="row">09.04.02</th>
                            <td><a class="text" href="pages/09.04.02.html">Информационные системы и технологии (интеллектуальные технологии обработки цифрового контента)</a></td>
                            <td>Комплексный экзамен по информационным системам и технологиям </td>
                        </tr>
                        <tr>
                            <th scope="row">15.04.02</th>
                            <td><a class="text" href="pages/15.04.02.html">технологические машины и оборудование (принтмедиасистемы и комплексы)</a></td>
                            <td>Комплексный экзамен по полиграфическому оборудованию</td>
                        </tr>
                        <tr>
                            <th scope="row">15.04.04</th>
                            <td><a class="text" href="pages/15.04.04.html">Автоматизация технологических процессов и производств (компьютерные системы сбора и обработки данных в принтмедиаиндустрии)</a></td>
                            <td>Комплексный экзамен по автоматизации и управлению</td>
                        </tr>
                        <tr>
                            <th scope="row">22.04.01</th>
                            <td><a class="text" href="pages/22.04.01.html">Материаловедение и технологии материалов (материаловедение и технологии материалов в полиграфии)</a></td>
                            <td>Комплексный экзамен по материаловедению и технологиям материалов</td>
                        </tr>
                        <tr>
                            <th scope="row">27.04.02.02</th>
                            <td><a class="text" href="pages/27.04.02.html">Управление качеством (управление в производственно-технологических системах)</a></td>
                            <td>Комплексный экзамен по материаловедению и технологиям материалов</td>
                        </tr>
                        
                        
                    </tbody>
                    </table>
                </div>
            </div>
    </div>
	<?php include("/home/std/site/template/footer.php");  ?>

</body>
</html>