<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0.5, maximum-scale=2, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>факультеты</title>
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="css/mdb.min.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>
<?php include("/home/std/site/template/header.php");  ?>
<main class="">
	<div id="headerImageContainer" class="container-fluid mb-5 p-0 bg-dark">
			<img id="headerImage" src="img/kek.jpg" class=""/>			
	</div>

	<div class="container-fluid mb-1" id="whypolitech">
		<div class="row">
			<div class="offset-md-4 col-md-4">
				<h2>Факультеты</h2>
			</div>
		</div>
	</div>
	

<div class="container-fluid mb-5">	

		<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/graphics/index.php" class="">
					<img class="card-img" src="img/21.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт графики и искусства книги</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/it/index.php" class="">
					<img class="card-img" src="img/22.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет информационных технологий </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/comunication/index.php" class="">
					<img class="card-img" src="img/23.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт коммуникаций и медиабизнеса</span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/usrists/index.php" class="">
					<img class="card-img" src="img/24.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Юридический факультет</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/journals/index.php" class="">
					<img class="card-img" src="img/25.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт издательского дела и журналистики</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/economics and control/index.php" class="">
					<img class="card-img" src="img/26.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет экономики и управления</span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/transport/index.php" class="">
					<img class="card-img" src="img/27.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Транспортный факультет </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/machine/index.php" class="">
					<img class="card-img" src="img/28.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет машиностроения</span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/printmedia/index.php" class="">
					<img class="card-img" src="img/29.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Институт принтмедиа и информационных технологий </span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
	
	<div class="card-deck-wrapper">	
		<div class="card-deck text-center">
			<div class="card view zoom" >
				<a href="pages/himbiotech/index.php" class="">
					<img class="card-img" src="img/30.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет химической технологии и биотехнологии </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/urbanistcs/index.php" class="">
					<img class="card-img" src="img/31.jpg" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0 bg-dark">
							<span class="card-text ">Факультет урбанистики и городского хозяйства </span>
						</div>
					</div>
				</a>
			</div>
			<div class="card view zoom" >
				<a href="pages/graphics/index.php" class="">
					<img class="card-img" src="" alt=""/>
					<div class="card-img-overlay p-0">
						<div class="p-0 m-0">
							<span class="card-text "></span>
						</div>
					</div>
				</a>
			</div>
		</div>	
	</div>
</div>
</main>
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>