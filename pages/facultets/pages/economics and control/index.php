<!DOCTYPE html>
<html>
<head>
	<title>Факультет экономики и управления</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel='stylesheet' id='style.css-css'  href='css/MDB Free/css/mdb.min.css' type='text/css' media='all' />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/26.jpg&quot;);">
		<div id="bg_pattern"></div>
	</div>
    
    <div class="container-fluid mb-5">
        <div class="row justify-content-md-center">
            <div class="col-12 text-center">
                <h1 class="text">Факультет экономики и управления</h1>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-10">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Код</th>
                            <th scope="col">Направление</th>
                            <th scope="col">Встпительные<br>испытания</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th colspan="3" scope="row">Бакалавриат и специалитет</th>
                        </tr>
                        <tr>
                            <th scope="row">38.03.01.01</th>
                            <td><a class="text" href="pages/38.03.01.01.html">Экономика (коммерциализация научно-технических разработок)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Обществознание</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">38.03.01.04</th>
                            <td><a class="text" href="pages/38.03.01.04.html">Экономика (бухгалтерский учет, анализ и аудит)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Обществознание</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">38.03.02.01</th>
                            <td><a class="text" href="pages/38.03.02.01.html">Менеджмент (менеджмент организации)</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Обществознание</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">38.03.03</th>
                            <td><a class="text" href="pages/38.03.03.html">Управление персоналом</a></td>
                            <td>
                                <ol>
                                    <li>Математика</li>
                                    <li>Обществознание</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">42.03.01.01</th>
                            <td><a class="text" href="pages/42.03.01.01.html">Реклама и связи с общественностью (бренд-менеджмент в рекламе и связях с общественностью)</a></td>
                            <td>
                                <ol>
                                    <li>Обществознание</li>
                                    <li>История</li>
                                    <li>Русский язык</li>
                                </ol>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
	<?php include("/home/std/site/template/footer.php");  ?>

</body>
</html>