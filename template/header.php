<link rel="stylesheet" type="text/css" href="/css/style.css">
<header>
	<nav class="navbar navbar-expand-md navbar-expand-lg fixed-top ">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse1" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon bg-dark text-white"></span>
        </button>
        <div class="collapse navbar-collapse p-0" id="navbarCollapse1">
			<div id="navFlex" class="navbar-nav text-center">
				<div class="nav-item">
					<a class="nav-link" href="/index.php">Главная</a>
				</div>
				<div class="nav-item">
					<a class="nav-link" href="/pages/facultets/index.php">Факультеты</a>
				</div>
				<div class="nav-item">
					<a class="nav-link" href="/pages/priem/index.php">Приемная коммисия</a>
				</div>
				<div class="nav-item">
					<a class="nav-link" href="http://mospolytech.ru/index.php?id=4275">Абитуриент онлайн</a>
				</div>
			</div>
		</div>
 	</nav>
</header>