<!DOCTYPE html>
<html>
<head>
	<title>Трудоустройство</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="utf-8">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	
</head>
<body>

	<?php include("/home/std/site/template/header.php");  ?>

	<div id="bbbblock" style="height: 37em; opacity: 1; background-image: url(&quot;img/1.png&quot;);">
		<div id="bg_pattern"></div>

	</div>

	<div style="height: 3%!important"></div>

	
	<div class="container-fluid">
		<div class="offset-md-4 col-md " style="color: #54a4d5;"><h1><b>Трудоустройство</b></h1></div>
			<div class="col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-sm-12 text-justify"><b style="font-size: 150%;">
Московский Политех предлагает своим студентам уникальную возможность планирования собственной карьеры, начиная с этапа поступления. Наша цель — чтобы с момента поступления в университет каждый первокурсник смог быстро адаптироваться в университетской среде, познакомился с кафедрами института, определился с планом развития на период обучения в вузе и к моменту получения диплома уже знал, куда он пойдет работать и кем.

Для этого мы проводим большую и системную работу с работодателями. Это сотрудничество основано на взаимном сотрудничестве: работодатели активно принимают участие в жизни университета, в организации профнавигационных и карьерных мероприятий, помогают нам корректировать программу обучения под реальные потребности производства, получая взамен высококвалифицированных специалистов, способных включиться в работу в минимальные сроки, готовые к изменениям и инновациям в технологиях.

Специально для этого создан интернет-портал «Электронный центр карьеры», который помогает студентам определиться с выбором профессионального развития, определить компанию своей мечты. В Центре собирается актуальная информация о вакансиях и оплачиваемых стажировках. Портал обеспечивает связь между сотрудниками, студентами и компаниями-партнерами университета. Благодаря использованию современных сетевых технологий студенты могут получить профессиональную консультацию, грамотно составить резюме, записаться на практику или устроиться на работу. Компании, в свою очередь, получают возможность подбирать квалифицированных кадров буквально с первого курса: следить за их достижениями в режиме онлайн, принимать участие в учебном процессе, учреждать именные стипендии, организовывать стажировки и практику.

Весь образовательный процесс в нашем университете тесно связан с практикой: студенты проходят стажировки, готовят совместные проекты и проводят исследования. Они решают не абстрактные задачи из учебников, а разработанные вместе с представителями бизнес-сообщества бизнес-кейсы, что позволяет осознать конкретные задачи, которые ставят перед ними будущие работодатели.

Выпускники Московского Политеха являются востребованными специалистами и получают достойную заработную плату, а начинается трудовая карьера обычно уже с третьего-четвёртого курса, когда студенты приступают к работе, используя те профессиональные навыки, которые приобретают в процессе обучения.

Нашими партнерами являются более 250 отечественных и зарубежных компаний и предприятий. Сотрудничество с ведущими предприятиями отрасли позволяет направить студентов на практику и помочь им с трудоустройством после окончания вуза.

Центр развития корпоративных связей устанавливает взаимодействие с кадровыми службами ведущих организаций машиностроительной отрасли в Москве, регионах России и за рубежом. Дважды в год, весной и осенью, в университете проводятся «Ярмарки вакансий», участниками которых

становятся крупнейшие отраслевые работодатели, а также кадровые агентства. Совместно с партнёрами вуза проводятся «Недели карьеры», мастер-классы, тренинги и презентации, организуются экскурсии на предприятия. 			</div>
	</div>
		
	
<?php include("/home/std/site/template/footer.php");  ?>
    <script src="js/jquery-3.3.1.min.js" ></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>